package com.patterns.prototype;

public class Square extends Shape {

	private int interiorAngleSum;

	public int getInteriorAngleSum() {
		return interiorAngleSum;
	}

	public void setInteriorAngleSum(int interiorAngleSum) {
		this.interiorAngleSum = interiorAngleSum;
	}
	
}
