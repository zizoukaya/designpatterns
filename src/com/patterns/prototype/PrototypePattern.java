package com.patterns.prototype;

public class PrototypePattern {

	public static void main(String[] args) {
		Registry registry = new Registry();
		Triangle triangle = (Triangle) registry.createShape("Triangle");
		triangle.setName("��gen1");
		
		System.err.println(triangle);		
		System.err.println(triangle.getEdgeNumber());		
		System.err.println(triangle.getName());		
		System.err.println(triangle.getType());
		
		Triangle triangle2 = (Triangle) registry.createShape("Triangle");
		triangle2.setName("��gen2");

		System.err.println(triangle2);		
		System.err.println(triangle2.getEdgeNumber());		
		System.err.println(triangle2.getName());		
		System.err.println(triangle2.getType());
	}
}
