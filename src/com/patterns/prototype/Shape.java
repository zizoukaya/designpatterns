package com.patterns.prototype;

public abstract class Shape implements Cloneable {

	private String name;
	private int edgeNumber;
	
	@Override
	protected Object clone() throws CloneNotSupportedException {
		return super.clone();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getEdgeNumber() {
		return edgeNumber;
	}
	public void setEdgeNumber(int edgeNumber) {
		this.edgeNumber = edgeNumber;
	}
	
}
