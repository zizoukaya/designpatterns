package com.patterns.prototype;

import java.util.HashMap;
import java.util.Map;

public class Registry {

	private Map<String, Shape> shapes = new HashMap<String, Shape>();
	
	public Registry() {
		loadShapes();
	}

	public Shape createShape(String name) {
		Shape shape = null;
		try {
			shape = (Shape) (shapes.get(name)).clone();
		} catch (CloneNotSupportedException e) {
			e.printStackTrace();
		}
		return shape;
	}
	
	private void loadShapes() {
		Triangle triangle = new Triangle();
		triangle.setEdgeNumber(3);
		triangle.setName("��gen");
		triangle.setType("equilateral");
		shapes.put("Triangle", triangle);
		
		Square square = new Square();
		square.setEdgeNumber(4);
		square.setInteriorAngleSum(360);
		square.setName("kare");
		shapes.put("Square", square);
	}

}
