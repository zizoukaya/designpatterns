package com.patterns.prototype;

public class Triangle extends Shape {

	private String type;

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}

}
