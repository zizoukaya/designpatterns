package com.patterns.builder;

public class BuilderPattern {

	public static void main(String args[]) {
		Person.Builder builder = new Person.Builder();
		builder.name("ahmet").surname("kaya");
		
		Person person = builder.build();
		
		System.out.println(person.getName());
		System.out.println(person.getSurname());
	}

}
