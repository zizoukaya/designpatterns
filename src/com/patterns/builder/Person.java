package com.patterns.builder;

public class Person {

	public static class Builder {
		private String name;
		private String surname;
		
		public Builder() {
			
		}
		
		public Person build() {
			return new Person(this);
		}

		public Builder name(String name) {
			this.name = name;
			return this;
		}
		
		public Builder surname(String surname) {
			this.surname = surname;
			return this;
		}
	}
	
	private final String name;
	private final String surname;
	
	public Person(Builder builder) {
		this.name = builder.name;
		this.surname = builder.surname;
	}

	public String getName() {
		return name;
	}

	public String getSurname() {
		return surname;
	}

	
}
