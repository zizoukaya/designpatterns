package com.patterns.factory;

public class Germany extends Country {

	@Override
	public void createShop() {
		cars.add(new Mercedes());
		cars.add(new Porsche());
	}

}
