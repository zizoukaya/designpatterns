package com.patterns.factory;

public class CountryFactory {

	public static Country getCountry(CountryType countryType) {
		switch (countryType) {
		case ITALY:
			return new Italy();
		case GERMANY:
			return new Germany();
		default:
			return null;
		}
	}

}
