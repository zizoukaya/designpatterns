package com.patterns.factory;

import java.util.Calendar;
import java.util.Locale;

public class FactoryPattern {

	public static void main(String[] args) {

		Calendar cal = Calendar.getInstance();
		System.out.println(cal);
		System.out.println(cal.get(Calendar.DAY_OF_MONTH));
		
		Country country = CountryFactory.getCountry(CountryType.ITALY);
		System.out.println(country.getCars());

		country = CountryFactory.getCountry(CountryType.GERMANY);
		System.out.println(country.getCars());
	}

}
