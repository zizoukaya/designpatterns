package com.patterns.abstractfactory;

public class CountryFactory extends CountryAbstractFactory {

	@Override
	public Country getCountry(CountryType countryType) {
		switch (countryType) {
		case ITALY:
			return new Italy();
		case GERMANY:
			return new Germany();
		default:
			return null;
		}
	}

	@Override
	public Validator getValidator(CountryType countryType) {
		switch (countryType) {
		case ITALY:
			return new ItalyValidator();
		case GERMANY:
			return new GermanyValidator();
		default:
			return null;
		}
	}

}
