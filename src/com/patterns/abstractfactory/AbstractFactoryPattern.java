package com.patterns.abstractfactory;

import java.io.ByteArrayInputStream;
import java.io.IOException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.xml.sax.SAXException;

public class AbstractFactoryPattern {

	public static void main(String[] args) throws SAXException, IOException, ParserConfigurationException {
//		String xml = "<document><body><num>123</num></body></document>";
//		ByteArrayInputStream bais = new ByteArrayInputStream(xml.getBytes());
//		
//		DocumentBuilderFactory abstractFactory = DocumentBuilderFactory.newInstance();
//		DocumentBuilder factory = abstractFactory.newDocumentBuilder();
//		Document doc = factory.parse(bais);
//		
//		doc.getDocumentElement().normalize();
//		System.out.println("Root Element : " + doc.getDocumentElement().getNodeName());
//		System.out.println(abstractFactory.getClass());
//		System.out.println(factory.getClass());
		
		
		CountryAbstractFactory myAbstractFactory = CountryAbstractFactory.getCountryAbstractFactory(100);
		Country country1 = myAbstractFactory.getCountry(CountryType.GERMANY);
		System.out.println(country1.getClass());

		CountryAbstractFactory myAbstractFactory2 = CountryAbstractFactory.getCountryAbstractFactory(20);
		Country country2 = myAbstractFactory2.getCountry(CountryType.ITALY);
		System.out.println(country2.getClass());
		
	}

}
