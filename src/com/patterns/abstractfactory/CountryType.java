package com.patterns.abstractfactory;

public enum CountryType {
	ITALY, GERMANY
}
