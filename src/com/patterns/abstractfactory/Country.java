package com.patterns.abstractfactory;

import java.util.ArrayList;
import java.util.List;

public abstract class Country {

	protected List<Car> cars = new ArrayList<Car>();
	
	public List<Car> getCars() {
		return cars;
	}
	
	public Country() {
		this.createShop();
	}
	
	public abstract void createShop();

}
