package com.patterns.abstractfactory;

public abstract class CountryAbstractFactory {

	public static CountryAbstractFactory getCountryAbstractFactory(int price) {
		if(price > 50) {
			return new CountryFactory();
		} else {
			return new CountryFactory2();
		}
	}

	public abstract Country getCountry(CountryType countryType);
	public abstract Validator getValidator(CountryType countryType);
}
