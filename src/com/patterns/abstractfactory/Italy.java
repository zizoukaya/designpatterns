package com.patterns.abstractfactory;

public class Italy extends Country {

	@Override
	public void createShop() {
		cars.add(new Ferrari());
		cars.add(new Maserati());
	}

}
