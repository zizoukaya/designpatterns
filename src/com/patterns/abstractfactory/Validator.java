package com.patterns.abstractfactory;

public interface Validator {
	public boolean isValid(Country country);
}
