package com.patterns.abstractfactory;

public class CountryFactory2 extends CountryAbstractFactory {

	@Override
	public Country getCountry(CountryType countryType) {
		switch (countryType) {
		case ITALY:
			return new Italy();
		case GERMANY:
			return new Germany();
		default:
			return null;
		}
	}

	@Override
	public Validator getValidator(CountryType countryType) {
		// TODO Auto-generated method stub
		return new GermanyValidator();
	}

}
