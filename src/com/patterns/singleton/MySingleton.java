package com.patterns.singleton;

public class MySingleton {

	private static MySingleton mySingleton = null;
	
	private MySingleton() {
		
	}
	
	public static MySingleton getInstance() {
		
		//Lazy Loading
		if(mySingleton == null) {
			//Thread Safe
			synchronized (MySingleton.class) {
				if(mySingleton == null) {
					mySingleton = new MySingleton();
				}
			}
		}
		
		return mySingleton;
	}

}
