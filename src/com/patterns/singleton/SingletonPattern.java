package com.patterns.singleton;

public class SingletonPattern {

	public static void main(String[] args) {

		Runtime singletonRuntime1 = Runtime.getRuntime();
		Runtime singletonRuntime2 = Runtime.getRuntime(); 

		if (singletonRuntime1.equals(singletonRuntime2)) {
			System.out.println("Singleton - Same Objects - singletonRuntime");
		} else {
			System.out.println("Not Singleton - Different Objects - singletonRuntime");
		}
		
		MySingleton mySingleton1 = MySingleton.getInstance();
		MySingleton mySingleton2 = MySingleton.getInstance();
		
		if (mySingleton1.equals(mySingleton2)) {
			System.out.println("Singleton - Same Objects - mySingleton");
		} else {
			System.out.println("Not Singleton - Different Objects - mySingleton");
		}
		
	}

}
